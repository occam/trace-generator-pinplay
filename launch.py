import os
import subprocess
import json

from occam import Occam

# Gather paths
scripts_path = os.path.dirname(__file__)
binary_path = "%s/pinplay-1.4-pin-2.14-67254-gcc.4.4.7-linux/pin" % (scripts_path)
so_path = "%s/Simulator/obj-intel64/TracerPin.so" % (scripts_path)

job_path = os.getcwd()
traces_path = "%s/traces" % (job_path)

object = Occam.load()
inputs = object.inputs()
outputs = object.outputs("trace")

if len(outputs) > 0:
  trace = outputs[0]
  traces_path = trace.volume()
else:
  os.mkdir(traces_path)

# Path to traces
trace_path = "%s/trace" % (traces_path)
num_instr = "100000"
start_instr = "100000"
process = "/bin/true"

#Get configuration options
data = object.configuration("PinPlay Options")


#Cycle through all options
for k, v in data.items():
  if k == "num_instr":
    num_instr = str(v)
  if k == "start_instr":
    start_instr = str(v)


if len(inputs) > 0:
  process = inputs[0].command()


# Form arguments
args = [binary_path,
		"-t", so_path,
        "-i", num_instr,
		"-s", start_instr,
		"-t", trace_path,
        "--", process]

# Form command line
command = ' '.join(args)

# Tell OCCAM how to run HMMSim
Occam.report(command)
