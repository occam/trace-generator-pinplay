apt-get update
echo 'y' | apt-get install unzip
wget http://people.cs.pitt.edu/~cem37/instrumentation.zip
unzip instrumentation.zip
tar -xvf pinplay-1.4-pin-2.14-67254-gcc.4.4.7-linux.tar.gz
export PIN_ROOT=`pwd`/pinplay-1.4-pin-2.14-67254-gcc.4.4.7-linux
wget http://people.cs.pitt.edu/~cem37/Simulator.zip
unzip Simulator.zip
cd Simulator
make
